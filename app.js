const express = require ('express');
const port = 8000;
const db = require ('./db/connection')

const app = express();
app.use(express.json())



app.post('/inserir', (req, res) => {
    const newcliente = req.body;
    const cmd_insert = 'INSERT INTO PEDIDOS SET?'
    db.query(cmd_insert,newcliente,(error,result) => {
        if(error) {
            res.status(400).json({message:"Informações inseridas é inválidas " + error}) 
        }else{
            res.status(201).json({message:"Novo pedido cadastrado com sucesso " + result.insertId}) 
        }
    })
})

app.get('/inserir/cpf/:cpf', (req, res) => {
    const buscacliente = req.params.cpf
    const busca = 'SELECT CODIGOPEDIDO, CPF, NOMEDOCLIENTE, VALORDOPEDIDO FROM PEDIDOS WHERE CPF=?'


    db.query(busca,buscacliente,(err, rows)=>{
        res.status(200).json(rows);
    })
})

app.put('/inserir/:codigopedido', (req, res) => {
    const codigopedido = req.params.codigopedido;
    const body = req.body;

    const sql = "UPDATE PEDIDOS SET VALORDOPEDIDO=? WHERE CODIGOPEDIDO=?"
    const values = [body.VALORDOPEDIDO, codigopedido]
    
    db.query(sql, values,(error,result)=>{
        if (error) {
            res.status(400).json({message:"Informações inseridas é inválidas " + error})
        }else{
            res.status(201).json({message: "O valor do pedido foi alterado com sucesso " })
        }
    })  
})

app.listen(port,()=>{
    console.log("-----------------------------")
    console.log(`Rodando na porta ${port}`)
    console.log("-----------------------------")
})